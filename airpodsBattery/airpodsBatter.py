import os
import re
import sys

class BluetoothBatteryPercentageGetter:
    def __init__(self, device_mac):
        self.mac = device_mac

    def load_data(self):
        stream = os.popen("defaults read /Library/Preferences/com.apple.Bluetooth DeviceCache")
        return stream.read()
    
    def process_data(self, data_text):
        re_result = re.search("\"" + self.mac + "\" = +{(.*\n)+?( )+};", data_text)
        device_text = data_text[re_result.start() : re_result.end()]
        re_battery = re.findall(r"BatteryPercent\w+ = \d+", device_text)
        return list(map(self.process_battery, re_battery))

    @staticmethod
    def process_battery(text):
        device = re.findall("[A-Z][a-z]+ ", text)[0][:-1]
        percentage = int(re.findall("\d+", text)[0])
        return (device, percentage)

    @staticmethod
    def remove_zeros(data):
        new_data = []
        for x in data:
            if x[1] > 0:
                new_data.append(x)
        return new_data

    @staticmethod
    def get_as_string(data):
        return " | ".join([ f"{x[0]}: {x[1]}%" for x in data])

    @staticmethod
    def get_as_emoji(processed):
        def get_emoji(data):
            p = data[1]
            if p < 10:
                return "💔"
            if p < 30:
                return "❤️"
            if p < 50:
                return "🧡"
            if p < 70:
                return "💛"
            return "💚"
        return " ".join(map(get_emoji, processed))

if __name__ == "__main__":
    device_mac = sys.argv[1]
    BtBPG = BluetoothBatteryPercentageGetter(device_mac)
    data = BtBPG.load_data()
    processed = BtBPG.process_data(data)

    if "-n" in sys.argv:
        processed = BtBPG.remove_zeros(processed)

    if ("-e" in sys.argv):
        print(BtBPG.get_as_emoji(processed))
    else:
        print(BtBPG.get_as_string(processed))

