# Airpods Battery

This simple script is made to print battery data from Airpods into terminal.

## Argument
This script takes only one argument and that is ID of your headphones.

## Flags
### `-n`
Remove items that have 0% or less. Reason for this is that case shows `0%` when headphones are connected.
### `-e`
Switch from text output into simple emoji output.

## Examples

| **Command** | **result** |
| --- | --- |
| `python3 airPodsBatter.py "e4-90-fd-93-5b-e2" -e -n` | `🧡 💛 ` |
| `python3 airPodsBatter.py "e4-90-fd-93-5b-e2"` | `Case: 0% \| Left: 47% \| Right: 49%` |


## How to get ID of your Airpods?
Use command:

`defaults read /Library/Preferences/com.apple.Bluetooth DeviceCache`

In returned data you may find your Airpods and their ID in format `xx-xx-xx-xx-xx-xx`. You don't need to have them connected at the moment.

### Tested with:
* Airpods Pro & OSx Catalina
* Airpods Pro & OSx Big Sur

### My usage
Personaly I use this script to run as widget on touchbar via [BetterTouchTool](https://folivora.ai/) with refresh rate of 10 sec. This way I always know battery level of my headphones.

### Contribute
Feel free to create issues or create pull requests. I will be happy to expand and improve this little piece of software.